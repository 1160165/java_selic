package app.model;

import app.model.repositories.RepositorioTaxas;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.junit.Assert.*;

@ExtendWith(MockitoExtension.class)
public class EstimativasTest {
    @Mock
    private Estimativas estimativas;
    @Mock
    private RepositorioTaxas repositorioTaxas;

    @BeforeEach
    public void init (){
        estimativas = new Estimativas(repositorioTaxas);
    }

    @Test
    public void importacaoFicheiro() throws IOException {
        //ARRANGE
        String path = "C:\\Users\\Vinicius\\Desktop\\ESTIMATIVA_SELIC.JSON";
        LeituraFicheiroJson importf = new LeituraFicheiroJson();
        //ACT
        TaxaSelic[] result = importf.importFromJson(path);
        assertEquals(132, result.length);
    }

}
