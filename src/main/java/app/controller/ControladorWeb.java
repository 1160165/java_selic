package app.controller;

import app.dto.ObjetoDto;
import app.model.Estimativas;
import app.model.TaxaSelic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.DecimalFormat;
import java.util.List;


@RestController
@RequestMapping(value = "/estimativas")
@CrossOrigin(origins = {"http://localhost:3000"}, maxAge = 3600)
public class ControladorWeb {

    private Estimativas estimativas;

    @Autowired
    public ControladorWeb(Estimativas estimativas) {
        this.estimativas = estimativas;
    }

    @PostMapping(value = "/importar")
    public ResponseEntity<Object> importarTaxas(@RequestBody ObjetoDto dto) {
        boolean listaDeTaxas = estimativas.importarTaxas(dto.getPath());
        if (listaDeTaxas)
            return new ResponseEntity<>("Ficheiro importado com sucesso", HttpStatus.OK);
        return new ResponseEntity<>("Não foi possível importar o ficheiro selecionado", HttpStatus.OK);
    }

    @GetMapping(value = "/taxa-mensal")
    public ResponseEntity<Object> getTaxaSelicGeral() {
        List<TaxaSelic> listaDeTaxas = estimativas.getListaDeTaxas();
        return new ResponseEntity<>(listaDeTaxas, HttpStatus.OK);
    }

    @PostMapping(value = "/taxa-mensal")
    public ResponseEntity<Object> getTaxaSelicMensal(@RequestBody ObjetoDto dto) {
        List<TaxaSelic> listaDeTaxas = estimativas.selicPorMes(dto.getData());
        return new ResponseEntity<>(listaDeTaxas, HttpStatus.OK);
    }

    @PostMapping(value = "/media-anual")
    public ResponseEntity<Object> getMediaTaxaSelicAnual(@RequestBody ObjetoDto dto) {
        double mediaSelicAnual = estimativas.mediaSelicAno(dto.getData());
        if (Double.compare(mediaSelicAnual, Double.NaN) == 0)
            return new ResponseEntity<>("Nenhuma leitura encontrada neste ano", HttpStatus.OK);
        return new ResponseEntity<>(new DecimalFormat("0.00").format(mediaSelicAnual), HttpStatus.OK);
    }
}
