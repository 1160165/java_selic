package app.model;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class LeituraFicheiroJson {


    public TaxaSelic[] importFromJson(String path) throws IOException {
        File file = new File(path);
        ObjectMapper objectMapper = new ObjectMapper();
        TaxaSelic[] readings = objectMapper.readValue(file, TaxaSelic[].class);

        return readings;

    }

}
