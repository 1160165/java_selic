package app.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;
import java.util.Date;

@Entity
public class TaxaSelic {

    @Id
    @JsonProperty("data_referencia")
    @Column(name="DataReferencia")
    private Date dataReferencia;
    @JsonProperty("estimativa_taxa_selic")
    @Column(name="TaxaSelic")
    private double estimativaTaxaSelic;

    public Date getDataReferencia() {
        return dataReferencia;
    }

    public double getEstimativaTaxaSelic() {
        return estimativaTaxaSelic;
    }
}
