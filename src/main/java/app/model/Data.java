package app.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Data {

    @Column(name="Ano")
    private int ano;
    @Column(name="Mes")
    private int mes;

    public Data() {
    }

    public Data(int ano, int mes) {
        this.ano = ano;
        this.mes = mes;
    }

    public Data(int ano) {
        this.ano = ano;
    }

    public int getAno() {
        return ano;
    }

    public int getMes() {
        return mes;
    }
}
