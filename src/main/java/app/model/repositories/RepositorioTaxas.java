package app.model.repositories;

import app.model.TaxaSelic;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioTaxas extends CrudRepository<TaxaSelic,Double> {

    List<TaxaSelic> findAll();

}
