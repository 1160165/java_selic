package app.model;


import app.model.repositories.RepositorioTaxas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@Service
public class Estimativas {

    private List<TaxaSelic> listaDeTaxas;
    private RepositorioTaxas repositorioTaxas;

    @Autowired
    public Estimativas(RepositorioTaxas repositorioTaxas) {
        this.repositorioTaxas = repositorioTaxas;
        this.listaDeTaxas = repositorioTaxas.findAll();
    }

    public List<TaxaSelic> getListaDeTaxas() {
        return listaDeTaxas;
    }

    public boolean importarTaxas(String path) {
        TaxaSelic[] listaDeTaxasImportadas;
        LeituraFicheiroJson leitura = new LeituraFicheiroJson();
        try {
            listaDeTaxasImportadas = leitura.importFromJson(path);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        for (TaxaSelic taxaLeitura : listaDeTaxasImportadas) {
            if (!listaDeTaxas.contains(taxaLeitura)) {
                repositorioTaxas.save(taxaLeitura);
            }
        }
        return true;
    }

    public List<TaxaSelic> selicPorMes(Data data) {
        List<TaxaSelic> listaDeTaxaSelicPorMes = new ArrayList<>();
        Calendar calendar = new GregorianCalendar();
        if (data.getMes() != 0)
            for (TaxaSelic taxaLeitura : listaDeTaxas) {
                calendar.setTime(taxaLeitura.getDataReferencia());
                if (calendar.get(Calendar.YEAR) == data.getAno())
                    if (calendar.get(Calendar.MONTH) + 1 == data.getMes())
                        listaDeTaxaSelicPorMes.add(taxaLeitura);
            }
        else listaDeTaxaSelicPorMes = listaDeTaxasSelicNoAno(data.getAno());
        return listaDeTaxaSelicPorMes;
    }

    private List<TaxaSelic> listaDeTaxasSelicNoAno(int ano) {
        Calendar calendar = new GregorianCalendar();

        List<TaxaSelic> listaDeTaxasSelicNoAno = new ArrayList<>();
        for (TaxaSelic taxa : listaDeTaxas) {
            calendar.setTime(taxa.getDataReferencia());
            if (calendar.get(Calendar.YEAR) == ano)
                listaDeTaxasSelicNoAno.add(taxa);
        }
        return listaDeTaxasSelicNoAno;
    }

    public double mediaSelicAno(Data data) {
        double estimativaTaxaSelicAno = 0;
        int contador = 0;
        Calendar calendar = new GregorianCalendar();
        for (TaxaSelic taxaSelic : listaDeTaxas) {
            calendar.setTime(taxaSelic.getDataReferencia());
            if (calendar.get(Calendar.YEAR) == data.getAno()) {
                estimativaTaxaSelicAno += taxaSelic.getEstimativaTaxaSelic();
                contador++;
            }
        }
        return estimativaTaxaSelicAno / contador;
    }
}
