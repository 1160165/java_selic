package app.dto;

import app.model.Data;

public class ObjetoDto {

    private String path;
    private Data data;

    public ObjetoDto() {
    }


    public String getPath() {
        return path;
    }

    public Data getData() {
        return data;
    }

}
